/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MPA_TOOLS_LOG_DEFS
#define MPA_TOOLS_LOG_DEFS

// directory to put logs in, used by voxl-logger and voxl-replay
#define LOG_BASE_DIR "/data/voxl-logger/";

// log types, more to be added later!!
#define TYPE_IMU 1
#define TYPE_CAM 2
#define TYPE_VIO 3
#define TYPE_TOF 4
#define TYPE_OV_EVAL 5
#define TYPE_QVIO 6
#define TYPE_POSE_6DOF 7
#define TYPE_VFT_FP 8
#define TYPE_RAW 9
#define TYPE_TOF2 10
#define TYPE_MAV 11

static const char* _type_to_string(int i) {
    if (i == TYPE_IMU) {
        return "imu";
    }
    if (i == TYPE_CAM) {
        return "cam";
    }
    if (i == TYPE_VIO) {
        return "vio";
    }
    if (i == TYPE_TOF) {
        return "tof";
    }
    if (i == TYPE_OV_EVAL) {
        return "ov_eval";
    }
    if (i == TYPE_QVIO) {
        return "qvio";
    }
    if (i == TYPE_POSE_6DOF) {
        return "pose_6dof";
    }
    if (i == TYPE_VFT_FP) {
        return "vft_fp";
    }
    if (i == TYPE_RAW) {
        return "raw";
    }
    if (i == TYPE_TOF2) {
        return "tof2";
    }
    if (i == TYPE_MAV) {
        return "mav";
    }
    return "unknown";
}

/*
 * Convert from Rotation matrix representing transformation from
 * frame 2 to frame 1.
 * The result will hold the angles defining the 3-2-1 intrinsic
 * Tait-Bryan rotation sequence from frame 1 to frame 2.
 * This is the usual nautical/aerospace order
 */
static void _rotation_to_tait_bryan(float R[3][3], float* roll, float* pitch,
                                    float* yaw) {
    *roll = atan2(R[2][1], R[2][2]);
    *pitch = asin(-R[2][0]);
    *yaw = atan2(R[1][0], R[0][0]);

    if (fabs((double)*pitch - M_PI_2) < 1.0e-3) {
        *roll = 0.0;
        *pitch = atan2(R[1][2], R[0][2]);
    } else if (fabs((double)*pitch + M_PI_2) < 1.0e-3) {
        *roll = 0.0;
        *pitch = atan2(-R[1][2], -R[0][2]);
    }
    return;
}

#define _ZERO_TOLERANCE 1e-8

static int rc_quaternion_normalize_array(double q[4]) {
    int i;
    double len;
    double sum = 0.0;
    for (i = 0; i < 4; i++) sum += q[i] * q[i];
    len = sqrtf(sum);

    // can't check if length is below a constant value as q may be filled
    // with extremely small but valid doubles
    if ((fabs(len) < _ZERO_TOLERANCE)) {
        fprintf(stderr, "ERROR in quaternion has 0 length\n");
        return -1;
    }
    for (i = 0; i < 4; i++) q[i] = q[i] / len;
    return 0;
}

static int rc_quaternion_from_tb_array(float tb[3], double q[4]) {
    if ((q == NULL || q == NULL)) {
        fprintf(
            stderr,
            "ERROR: in rc_quaternion_from_tb_array, received NULL pointer\n");
        return -1;
    }

    double tbt[3];
    tbt[0] = (double)tb[0] / 2.0;
    tbt[1] = (double)tb[1] / 2.0;
    tbt[2] = (double)tb[2] / 2.0;
    double cosX2 = cos(tbt[0]);
    double sinX2 = sin(tbt[0]);
    double cosY2 = cos(tbt[1]);
    double sinY2 = sin(tbt[1]);
    double cosZ2 = cos(tbt[2]);
    double sinZ2 = sin(tbt[2]);
    q[0] = cosX2 * cosY2 * cosZ2 + sinX2 * sinY2 * sinZ2;
    q[1] = sinX2 * cosY2 * cosZ2 - cosX2 * sinY2 * sinZ2;
    q[2] = cosX2 * sinY2 * cosZ2 + sinX2 * cosY2 * sinZ2;
    q[3] = cosX2 * cosY2 * sinZ2 - sinX2 * sinY2 * cosZ2;
    rc_quaternion_normalize_array(q);
    return 0;
}

/*
 * Convert from Rotation matrix representing transformation from
 * frame 2 to frame 1.
 * The result will hold the angles defining the 1-2-3 intrinsic
 * Tait-Bryan rotation sequence from frame 1 to frame 2.
 * This is the order used for imu-camera extrinsic
 */
static void _rotation_to_tait_bryan_xyz_intrinsic(float R[3][3], float* roll,
                                                  float* pitch, float* yaw) {
    *pitch = asin(R[0][2]);
    if (fabs(R[0][2]) < 0.999f) {
        *roll = atan2(-R[1][2], R[2][2]);
        *yaw = atan2(-R[0][1], R[0][0]);
    } else {
        *roll = atan2(R[2][1], R[1][1]);
        *yaw = 0.0f;
    }
    return;
}

#define DEG_TO_RAD  (3.14159265358979323846/180.0)
#define RAD_TO_DEG  (180.0/3.14159265358979323846)

#endif  // MPA_TOOLS_LOG_DEFS
