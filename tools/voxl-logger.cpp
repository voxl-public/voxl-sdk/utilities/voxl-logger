/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <assert.h>
#include <cJSON.h>
#include <c_library_v2/common/mavlink.h>
#include <c_library_v2/mavlink_types.h>
#include <errno.h>
#include <getopt.h>
#include <inttypes.h> /* for PRIu64 */
#include <limits.h>
#include <locale.h> /* for setlocale() */
#include <modal_json.h>
#include <modal_pipe_client.h>
#include <modal_pipe_interfaces.h>
#include <modal_start_stop.h>

#ifndef BUILD_APQ8096
#include <vft_interface.h>
#endif

#include <png.h>
#include <stdint.h> /* for uint_types */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>  // for mkdir
#include <sys/types.h>
#include <time.h>
#include <turbojpeg.h>
#include <unistd.h>
#include <unistd.h>  // for access()
#include <zlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <arpa/inet.h> // For htonl()

#include <cmath>
#include <fstream>
#include <limits>
#include <sstream>
#include <vector>

#include "common.h"
#include "log_defs.h"

using namespace std;

#define THROW(action, message) \
    { printf("ERROR in line %d while %s:\n%s\n", __LINE__, action, message); }

#define THROW_TJ(action) THROW(action, tjGetErrorStr2(tjInstance))

#define THROW_UNIX(action) THROW(action, strerror(errno))

#define PIPE_CLIENT_NAME "voxl-logger"  // client name for pipes

#define MAV_STATUS_CH   98



typedef struct channel_t {
    int running;
    int type;
    pthread_t thread_id;
    char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
    int sample_limit;
    int n_samples;
    int n_to_skip;
    int n_skipped;
    int dropped_data;
    FILE* csv_fd;
    FILE* raw_fd;
} channel_t;

// array of each channel state
static channel_t c[PIPE_CLIENT_MAX_CHANNELS];

// optional user-defined note to be included in the log json file
#define MAX_NOTE_LEN 128
static char note[MAX_NOTE_LEN] = "na";

static char base_dir[128] = LOG_BASE_DIR;
static char info_path[512];

// this is the complete path beginning with base_dir into which all log data
// in included, for example: /data/voxl-logger/log0001/
static char log_dir[256];

// number of channels enabled by user
static int n_ch;

// optional debug mode
static int en_debug;

// timeout set positive by command line arg indicates timeout feature is on
static float timeout_s = -1;
static pthread_t timeout_thread;
int64_t start_time;

// only record at arm time like other Mavlink Apps
static bool is_armed = false;
static bool is_operating = false;
static bool only_record_on_arm = false;

// json objects
cJSON* parent;
cJSON* channel_array;
cJSON* duration_json;

// printed if some invalid argument was given
static void _print_usage(void) {
    printf(
        "\n\
Tool to save data published through Modal Pipe Architecture to disk.\n\
By default, this saves images to /data/voxl-logger/ since the /data/ parition is\n\
the largest partition on VOXL. You can change this with the -d argument.\n\
\n\
-c, --cam {name}            name of a camera to log, e.g. tracking.\n\
                              greyscale images will be saved as a lossless png.\n\
                              color images as jpg unless --color-png is set\n\
-d, --directory {dir}       name of the directory to save the log to, default\n\
                              is /data/voxl-logger/ if argument is not provided.\n\
-f, --pose_6dof {name}      name of a pose6DOF topic to log, e.g. vvpx4_attitude\n\
-i, --imu {name}            name of an imu to log, e.g. imu1.\n\
-k, --skip {n_to_skip}      number of samples to skip between logged samples\n\
-h, --help                  print this help message\n\
-n, --note {note}           optionally add a very short note to the end of the log\n\
                              directory name, e.g. flight_with_new_motors\n\
-o, --preset_odometry       record a preset log containing qvio, ovins, vft, tracking cam and \n\
                              IMUs for testing VIO. This can be used with other pipes.\n\
-m, --mavlink {name}        name of a mavlink pipe.\n\
-p, --tof {name}            name of a PMD TOF pipe.\n\
-q, --qvio                  log the extended qvio data including feature points and covarience\n");
#ifndef BUILD_APQ8096
printf("-r, --feature_packet        log vft_feature_packet\n");
#endif
printf("\
-s, --samples {samples}     maximum samples to save for one specific channel,\n\
                              otherwise it will run untill you quit the program.\n\
-t, --time {seconds}        time to log for in seconds.\n\
-v, --vio {name}            name of a vio pipe to log, e.g. qvio\n\
-w, --raw                   log raw binary data to data.raw file\n\
-z, --debug                 enable verbose debug mode\n\
-a, --armed                 only record to log when armed\n\
\n\
typical uses:\n\
  To log one camera image:\n\
    yocto:/# voxl-logger --cam tracking --samples 1 \n\
\n\
  To log 5000 imu samples:\n\
    yocto:/# voxl-logger -i imu1 -s 5000 --note \"primary imu test\"\n\
\n\
  To log 1000 imu0 samples and 2000 imu1 samples:\n\
    yocto:/# voxl-logger -i imu0 -s 1000 -i imu1 -s 2000\n\
\n\
  To log every 5th imu sample (skip 4) for 5.5 seconds:\n\
    yocto:/# voxl-logger -i imu1 --skip 4 -t 5.5\n\
\n\
  To log tracking camera and both imus until you press ctrl-c\n\
    yocto:/# voxl-logger --preset_odometry\n\
\n\
  To record a typical log for testing VIO (imu + tracking) for 1 minute\n\
    yocto:/# voxl-logger --preset_odometry --time 60 --note \"log for vio replay test 1\"\n\
\n\
\n");
    return;
}

static int _mkdir(const char* dir) {
    char tmp[PATH_MAX];
    char* p = NULL;

    snprintf(tmp, sizeof(tmp), "%s", dir);
    for (p = tmp + 1; *p != 0; p++) {
        if (*p == '/') {
            *p = 0;
            if (mkdir(tmp, S_IRWXU) && errno != EEXIST) {
                perror("ERROR calling mkdir");
                printf("tried to mkdir %s\n", tmp);
                return -1;
            }
            *p = '/';
        }
    }
    return 0;
}

static int _add_channel(int type, char* optarg) {
    // start a new channel and log that we have a new channel enabled
    if (n_ch >= PIPE_CLIENT_MAX_CHANNELS) {
        fprintf(stderr, "Too many channels requested!\n");
        return -1;
    }
    if (pipe_expand_location_string(optarg, c[n_ch].pipe_path) < 0) {
        fprintf(stderr, "Invalid pipe name: %s\n", optarg);
        return -1;
    }

    // check if the path has already been added
    for (int i = 0; i < n_ch; i++) {
        if (strcmp(c[n_ch].pipe_path, c[i].pipe_path) == 0) {
            printf(
                "Warning: the following pipe was requested multiple times:\n");
            printf("%s\n", c[n_ch].pipe_path);
            return 0;
        }
    }

    c[n_ch].type = type;
    n_ch++;
    return 0;
}

static int _parse_opts(int argc, char* argv[]) {
    static struct option long_options[] = {
        {"cam", required_argument, 0, 'c'},
        {"directory", required_argument, 0, 'd'},
        {"ov_eval", required_argument, 0, 'e'},
        {"pose_6dof", required_argument, 0, 'f'},
        {"imu", required_argument, 0, 'i'},
        {"skip", required_argument, 0, 'k'},
        {"help", no_argument, 0, 'h'},
        {"note", required_argument, 0, 'n'},
        {"mavlink", required_argument, 0, 'm'},
        {"preset_odometry", no_argument, 0, 'o'},
        {"qvio", no_argument, 0, 'q'},
        #ifndef BUILD_APQ8096
        {"feature_packet", required_argument, 0, 'r'},
        #endif
        {"tof", required_argument, 0, 'p'},
        {"samples", required_argument, 0, 's'},
        {"time", required_argument, 0, 't'},
        {"vio", required_argument, 0, 'v'},
        {"raw", required_argument, 0, 'w'},
        {"debug", no_argument, 0, 'z'},
        {"armed", no_argument, 0, 'a'},
        {0, 0, 0, 0}};

    // we are going to set up the channels in sequence based on the order of
    // arguments as the user provided them. Start at 0 indicating the user
    // hasn't specified any channels yet. Once the first channel starts, this
    // will increase

    while (1) {
        int len, n, notelen;
        int option_index = 0;
        int j = getopt_long(argc, argv, "c:d:e:f:i:k:hn:m:aoqr:p:s:t:v:w:z",
                            long_options, &option_index);

        if (j == -1) break;  // Detect the end of the options.

        switch (j) {
            case 0:
                // for long args without short equivalent that just set a flag
                // nothing left to do so just break.
                if (long_options[option_index].flag != 0) break;
                break;

            case 'c':
                if (_add_channel(TYPE_CAM, optarg) < 0) return -1;
                break;

            case 'd':
                // make the directory if necessary
                if (_mkdir(optarg)) return -1;
                sprintf(base_dir, "%s", optarg);
                len = strlen(base_dir);
                // make sure directory ends in a trailing '/'
                if (base_dir[len] != '/') {
                    base_dir[len] = '/';
                    base_dir[len + 1] = 0;
                }
                break;

            case 'e':
                if (_add_channel(TYPE_OV_EVAL, optarg) < 0) return -1;
                break;

            case 'f':
                if (_add_channel(TYPE_POSE_6DOF, optarg) < 0) return -1;
                break;

            case 'i':
                if (_add_channel(TYPE_IMU, optarg) < 0) return -1;
                break;
            #ifndef BUILD_APQ8096
            case 'r':
                if (_add_channel(TYPE_VFT_FP, optarg) < 0) return -1;
                break;
            #endif

            case 'w':
                if (_add_channel(TYPE_RAW, optarg) < 0) return -1;
                break;

            case 'k':
                if (n_ch < 1) {
                    fprintf(stderr,
                            "ERROR: you must specify the skip arg after the "
                            "respective pipe name\n");
                    return -1;
                }
                n = atoi(optarg);
                if (n < 0) {
                    fprintf(stderr,
                            "ERROR: number of samples to skip must be >=0\n");
                    return -1;
                }
                c[n_ch - 1].n_to_skip = n;
                break;

            case 'h':
                _print_usage();
                return -1;
                break;

            case 'a':
                only_record_on_arm = true;
                break;

            case 'n':
                notelen = strlen(optarg);
                if (notelen < 1) {
                    fprintf(stderr, "ERROR: empty note string provided\n");
                    return -1;
                }
                if (notelen >= MAX_NOTE_LEN) {
                    fprintf(stderr,
                            "ERROR: note string must be < %d characters\n",
                            MAX_NOTE_LEN);
                    return -1;
                }
                if (strchr(optarg, '/') != NULL) {
                    fprintf(stderr, "ERROR: note string can't contain '/'\n");
                    return -1;
                }
                // replace all spaces in the note with an underscore
                for (int c = 0; c < notelen; c++) {
                    if (optarg[c] == ' ') optarg[c] = '_';
                }
                strcpy(note, optarg);
                break;

            case 'o':
                if(_add_channel(TYPE_IMU, (char*)"imu_apps") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"tracking") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"tracking_misp_norm") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"trackingL") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"trackingL_misp_norm") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"trackingR") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"trackingR_misp_norm") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"tracking_front") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"tracking_front_misp_norm") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"tracking_down") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"tracking_down_misp_norm") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"tracking_rear") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"tracking_rear_misp_norm") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"qvio_overlay") < 0) return -1;
                if(_add_channel(TYPE_VIO, (char*)"qvio") < 0) return -1;
                if(_add_channel(TYPE_CAM, (char*)"ov_overlay")<0)return -1;
                if(_add_channel(TYPE_VIO, (char*)"ov")<0)return -1;
                if(_add_channel(TYPE_MAV, (char*)"mavlink_onboard") < 0) return -1;
                if(_add_channel(TYPE_VIO, (char*)"vvhub_aligned_vio")<0)return -1;
                break;

            case 'q':
                if (_add_channel(TYPE_QVIO, (char*)"qvio_extended") < 0) return -1;
                break;

            case 'p':
                // Note that this just becomes TYPE_TOF for now, callback will
                // reassign this
                if (_add_channel(TYPE_TOF, optarg) < 0) return -1;
                break;

            case 'm':
                if (_add_channel(TYPE_MAV, optarg) < 0) return -1;
                break;

            case 's':
                if (n_ch < 1) {
                    fprintf(stderr,
                            "ERROR: you must specify the samples arg after the "
                            "respective pipe name\n");
                    return -1;
                }
                n = atoi(optarg);
                if (n < 1) {
                    fprintf(stderr,
                            "ERROR: number of samples must be positive\n");
                    return -1;
                }
                c[n_ch - 1].sample_limit = n;
                break;

            case 't':
                timeout_s = atof(optarg);
                if (timeout_s < 0.5f) {
                    fprintf(stderr, "ERROR: timeout must be >=0.5 seconds\n");
                    return -1;
                }
                break;

            case 'v':
                if (_add_channel(TYPE_VIO, optarg) < 0) return -1;
                break;

            case 'z':
                printf("enabling debug mode\n");
                en_debug = 1;
                break;

            default:
                _print_usage();
                return -1;
        }
    }

    if (n_ch < 1) {
        fprintf(stderr, "ERROR: at least one pipe channel must be specified\n");
        return -1;
    }

    if (en_debug) printf("user requested %d channels\n", n_ch);

    return 0;
}

static int _start_csv(int ch) {
    int i;

    // construct the data path
    char csv_path[512];
    char raw_path[512];
    sprintf(csv_path, "%s%sdata.csv", log_dir, c[ch].pipe_path);
    if (en_debug) {
        printf("making new csv file for channel %d: %s\n", ch, csv_path);
    }

    // make any required subdirs and the csv itself
    _mkdir(csv_path);
    FILE* fd = fopen(csv_path, "w+");
    if (fd == 0) {
        fprintf(stderr, "ERROR: can't open log file for writing\n");
        c[ch].running = 0;
        return -1;
    }

    // write header
    int ret = 0;
    if (c[ch].type == TYPE_IMU) {
        ret = fprintf(fd,
                      "i,timestamp(ns),batch_id,AX(m/s2),AY(m/s2),AZ(m/s2),GX(rad/"
                      "s),GY(rad/s),GZ(rad/s),T(C)\n");
    } else if (c[ch].type == TYPE_CAM) {
        ret = fprintf(fd,
                      "i,timestamp(ns),gain,exposure(ns),format,height,width,"
                      "frame_id,reserved\n");
    } else if (c[ch].type == TYPE_VIO) {
        fprintf(fd, "i,timestamp(ns),");
        fprintf(fd,
                "T_imu_wrt_vio_x(m),T_imu_wrt_vio_y(m),T_imu_wrt_vio_z(m),");
        fprintf(fd, "roll(rad),pitch(rad),yaw(rad),");
        fprintf(fd,
                "vel_imu_wrt_vio_x(m/s),vel_imu_wrt_vio_y(m/"
                "s),vel_imu_wrt_vio_z(m/s),");
        fprintf(
            fd,
            "angular_vel_x(rad/s),angular_vel_y(rad/s),angular_vel_z(rad/s),");
        fprintf(fd,
                "gravity_vector_x(m/s2),gravity_vector_y(m/"
                "s2),gravity_vector_z(m/s2),");
        fprintf(fd,
                "T_cam_wrt_imu_x(m),T_cam_wrt_imu_y(m),T_cam_wrt_imu_z(m),");
        fprintf(fd,
                "imu_to_cam_roll_x(rad),imu_to_cam_pitch_y(rad),imu_to_cam_yaw_"
                "z(rad),");
        ret = fprintf(fd, "features,quality,state,error_code\n");
    } else if (c[ch].type == TYPE_TOF || c[ch].type == TYPE_TOF2 ||
               c[ch].type == TYPE_MAV) {
        // first create csv
        ret = fprintf(fd, "i,timestamp(ns)\n");
        // then the raw file
        sprintf(raw_path, "%s%sdata.raw", log_dir, c[ch].pipe_path);
        FILE* raw_fd = fopen(raw_path, "w+");
        if (raw_fd == NULL) {
            fprintf(stderr, "ERROR: can't open raw file for writing\n");
            return -1;
        }
        c[ch].raw_fd = raw_fd;
    } else if (c[ch].type == TYPE_OV_EVAL) {
        ret = fprintf(fd, "time,x,y,z,qx,qy,qz,qw\n");
    } else if (c[ch].type == TYPE_POSE_6DOF) {
        fprintf(fd, "i,timestamp(ns),");
        fprintf(fd, "T_ch_wrt_par_x(m),T_ch_wrt_par_y(m),T_ch_wrt_par_z(m),");
        fprintf(fd, "roll(rad),pitch(rad),yaw(rad),");
        fprintf(fd,
                "vel_ch_wrt_par_x(m/s),vel_ch_wrt_par_y(m/"
                "s),vel_ch_wrt_par_z(m/s),");
        ret = fprintf(
            fd,
            "angular_vel_x(rad/s),angular_vel_y(rad/s),angular_vel_z(rad/s)\n");
    } else if (c[ch].type == TYPE_QVIO) {
        fprintf(fd, "quality,timestamp_ns,");
        fprintf(fd, "frame_id, frame_timestamp_ns,");
        fprintf(fd, "T_imu_wrt_vio_x,T_imu_wrt_vio_y,T_imu_wrt_vio_z,");
        fprintf(fd, "roll(rad),pitch(rad),yaw(rad),");

        fprintf(fd,
                "T_ga_vio_wrt_cam_x,T_ga_vio_wrt_cam_y,T_ga_vio_wrt_cam_z,");
        fprintf(fd,
                "R_ga_vio_to_cam_roll(rad),R_ga_vio_to_cam_pitch(rad),R_ga_vio_"
                "to_cam_yaw(rad),");

        fprintf(fd,
                "pose_cov_00,pose_cov_11,pose_cov_22,pose_cov_33,pose_cov_44,"
                "pose_cov_55,");
        fprintf(fd, "imu_cam_time_shift_s,");

        fprintf(fd, "vel_imu_wrt_vio_x,vel_imu_wrt_vio_y,vel_imu_wrt_vio_z,");
        fprintf(fd, "vel_cov_00,vel_cov_11,vel_cov_22,");

        fprintf(
            fd,
            "angular_vel_x(rad/s),angular_vel_y(rad/s),angular_vel_z(rad/s),");
        fprintf(fd, "gravity_vector_x,gravity_vector_y,gravity_vector_z,");
        fprintf(fd, "grav_cov_00,grav_cov_11,grav_cov_22,");

        fprintf(fd, "gyro_bias_x,gyro_bias_y,gyro_bias_z,");
        fprintf(fd, "accl_bias_x,accl_bias_y,accl_bias_z,");

        fprintf(fd, "T_cam_wrt_imu_x,T_cam_wrt_imu_y,T_cam_wrt_imu_z,");
        fprintf(fd, "imu_to_cam_roll_x,imu_to_cam_pitch_y,imu_to_cam_yaw_z,");

        fprintf(fd, "error_code, n_feature_points, state, n_total_features,");

        // group the feature quality, depth, and location at the beginning to
        // make reading the log in a spreadsheet easier
        for (i = 0; i < VIO_MAX_REPORTED_FEATURES; i++) {
            fprintf(fd, "fp_qual_%d,", i);
        }
        for (i = 0; i < VIO_MAX_REPORTED_FEATURES; i++) {
            fprintf(fd, "fp_depth_%d,", i);
        }
        for (i = 0; i < VIO_MAX_REPORTED_FEATURES; i++) {
            fprintf(fd, "fp_depth_err%d,", i);
        }
        for (i = 0; i < VIO_MAX_REPORTED_FEATURES; i++) {
            fprintf(fd, "fp_loc_x_%d,fp_loc_y_%d,", i, i);
        }

        // dump the rest of the data at the end
        for (i = 0; i < VIO_MAX_REPORTED_FEATURES; i++) {
            fprintf(fd, "fp_id_%d,", i);
            fprintf(fd, "fp_tsf_x_%d,fp_tsf_y_%d,fp_tsf_z_%d", i, i, i);
            fprintf(fd, "fp_p_tsf_x_%d,fp_p_tsf_y_%d,fp_p_tsf_z_%d", i, i, i);
        }

        ret = fprintf(fd, "\n");
    }
    #ifndef AP8096
    else if (c[ch].type == TYPE_VFT_FP) {
        fprintf(fd, "timestamp(ns),reset,frame_id,cam_id,id,x(pix),y(pix),");
        fprintf(fd, "prev_x,prev_y,age,score,pyr_lvl_mask,descriptor\n");
    }
    #endif
    else if (c[ch].type == TYPE_RAW) {
        fprintf(fd, "i,num_bytes\n");
        // then the raw file
        sprintf(raw_path, "%s%sdata.raw", log_dir, c[ch].pipe_path);
        FILE* raw_fd = fopen(raw_path, "w+");
        if (raw_fd == NULL) {
            fprintf(stderr, "ERROR: can't open raw file for writing\n");
            return -1;
        }
        c[ch].raw_fd = raw_fd;
    }

    if (ret < 0) {
        perror("failed to write csv header");
        return -1;
    }

    // save fd for writing to later
    c[ch].csv_fd = fd;
    return 0;
}

static void _write_int(int ch, int i) {
    fprintf(c[ch].csv_fd, "%d,", i);
    return;
}

static void _write_int64(int ch, int64_t i) {
    fprintf(c[ch].csv_fd, "%ld,", i);
    return;
}

static void _write_float(int ch, float v) {
    if (v == 0.0f) {
        fprintf(c[ch].csv_fd, "0,");
    } else if (v == -1.0f) {
        fprintf(c[ch].csv_fd, "-1,");
    } else {
        fprintf(c[ch].csv_fd, "%0.3f,", (double)v);
    }
    return;
}

static void _write_float_3(int ch, float* v) {
    for (int i = 0; i < 3; i++) {
        if (v[i] == 0.0f) {
            fprintf(c[ch].csv_fd, "0,");
        } else {
            fprintf(c[ch].csv_fd, "%0.3f,", (double)v[i]);
        }
    }
    return;
}

static void _write_cov_3x3(int ch, float v[3][3]) {
    for (int i = 0; i < 3; i++) {
        if (v[i][i] == 0.0f) {
            fprintf(c[ch].csv_fd, "0,");
        } else {
            fprintf(c[ch].csv_fd, "%0.3e,", (double)v[i][i]);
        }
    }
    return;
}

static void _write_cov_6x6(int ch, float v[6][6]) {
    for (int i = 0; i < 6; i++) {
        if (v[i][i] == 0.0f) {
            fprintf(c[ch].csv_fd, "0,");
        } else {
            fprintf(c[ch].csv_fd, "%0.3e,", (double)v[i][i]);
        }
    }
    return;
}

static void _write_cov_6x6_tri(int ch, float v[21]) {
    fprintf(c[ch].csv_fd, "%0.3e,", (double)v[0]);
    fprintf(c[ch].csv_fd, "%0.3e,", (double)v[6]);
    fprintf(c[ch].csv_fd, "%0.3e,", (double)v[11]);
    fprintf(c[ch].csv_fd, "%0.3e,", (double)v[15]);
    fprintf(c[ch].csv_fd, "%0.3e,", (double)v[18]);
    fprintf(c[ch].csv_fd, "%0.3e,", (double)v[20]);

    return;
}

static void _write_cov_3x3_tri(int ch, float v[21]) {
    fprintf(c[ch].csv_fd, "%0.3e,", (double)v[0]);
    fprintf(c[ch].csv_fd, "%0.3e,", (double)v[6]);
    fprintf(c[ch].csv_fd, "%0.3e,", (double)v[11]);

    return;
}

static void _write_R_as_tb(int ch, float R[3][3]) {
    float roll, pitch, yaw;
    _rotation_to_tait_bryan(R, &roll, &pitch, &yaw);
    _write_float(ch, roll);
    _write_float(ch, pitch);
    _write_float(ch, yaw);

    return;
}

static void _write_R_as_tb_xyz_intrinsic(int ch, float R[3][3]) {
    float roll_cam, pitch_cam, yaw_cam;
    _rotation_to_tait_bryan_xyz_intrinsic(R, &roll_cam, &pitch_cam, &yaw_cam);
    _write_float(ch, roll_cam);
    _write_float(ch, pitch_cam);
    _write_float(ch, yaw_cam);

    return;
}

static void _simple_cb(int ch, char* data, int bytes,
                       __attribute__((unused)) void* context) {
    int i, j;

    // reading from a pipe will return 0 if the other end (server) closes
    // check for this (and negative  numbers indicating error) and quit.
    // your program doesn't need to quit, you can handle the server quitting
    // however you like to suite your needs. We just quit for this example.
    if (bytes <= 0) {
        fprintf(stderr, "Server Disconnected from channel %d\n", ch);
        c[ch].running = 0;
        return;
    }

    // if we've been told to stop running, just return
    if (c[ch].running == 0 || main_running == 0) return;

    if (only_record_on_arm && !is_armed)
        return;

    // if file hasn't been updated yet, open it!
    if (c[ch].csv_fd == 0) {
        if (_start_csv(ch)) {
            return;
        }
    }

    // validate that the data makes sense
    int n_received;
    imu_data_t* imu_array;
    static int imu_batch_idx = 0;
    vio_data_t* vio_array;
    tof_data_t* tof_array;
    tof2_data_t* tof2_array;
    mavlink_message_t* mavlink_array;
    ext_vio_data_t* ext_vio_array;
    pose_vel_6dof_t* pose_6dof_array;
    #ifndef BUILD_APQ8096
    vft_feature_packet* vft_feat_packet;
    vft_feature* vft_feats;
    #endif

    if (c[ch].type == TYPE_IMU) {
        imu_array = pipe_validate_imu_data_t(data, bytes, &n_received);
        imu_batch_idx++;
        if (en_debug) {
            printf("read %d IMU packets from ch: %d\n", n_received, ch);
        }
        if (imu_array == NULL) return;
        if (n_received <= 0) return;
    } else if (c[ch].type == TYPE_VIO || c[ch].type == TYPE_OV_EVAL) {
        vio_array = pipe_validate_vio_data_t(data, bytes, &n_received);
        if (en_debug) {
            printf("read %d VIO packets from ch: %d\n", n_received, ch);
        }
        if (vio_array == NULL) return;
        if (n_received <= 0) return;
    } else if (c[ch].type == TYPE_POSE_6DOF) {
        pose_6dof_array =
            pipe_validate_pose_vel_6dof_t(data, bytes, &n_received);
        if (en_debug) {
            printf("read %d Pose6DOF packets from ch: %d\n", n_received, ch);
        }
        if (pose_6dof_array == NULL) return;
        if (n_received <= 0) return;
    } else if (c[ch].type == TYPE_TOF || c[ch].type == TYPE_TOF2) {
        // NOTE: TYPE_TOF is deprecated but still included for backwards
        // compatiblity
        if (bytes % sizeof(tof_data_t)) {
            if (c[ch].type == TYPE_TOF)
                c[ch].type = TYPE_TOF2;  // update if wrong type from --tof arg
            tof2_array = pipe_validate_tof2_data_t(data, bytes, &n_received);
            if (tof2_array == NULL) return;
        } else {
            // this throws a warning that this function is deprecated, which it
            // is, but we keep it here for backwards compatability for now.
            tof_array = pipe_validate_tof_data_t(data, bytes, &n_received);
            if (tof_array == NULL) return;
        }
        if (en_debug) {
            printf("read %d TOF packets from ch: %d\n", n_received, ch);
        }
        if (n_received <= 0) return;
    } else if (c[ch].type == TYPE_MAV) {
        mavlink_array =
            pipe_validate_mavlink_message_t(data, bytes, &n_received);
        if (en_debug) {
            printf("read %d mavlink packets from ch: %d\n", n_received, ch);
        }
        if (n_received <= 0) return;
    } else if (c[ch].type == TYPE_QVIO) {
        ext_vio_array = pipe_validate_ext_vio_data_t(data, bytes, &n_received);
        if (en_debug) {
            printf("read %d EXT VIO packets from ch: %d\n", n_received, ch);
        }
        if (ext_vio_array == NULL) return;
        if (n_received <= 0) return;
    }
    #ifndef BUILD_APQ8096
    else if (c[ch].type == TYPE_VFT_FP) {
        create_vft_memory(&vft_feat_packet, &vft_feats);
        if (validate_vft_data(ch, data, bytes, &vft_feat_packet, &vft_feats)) {
            printf("ERROR parsing vft packet from pipe...\n");
            return;
        }
        n_received = 0;
        for (int i = 0; i < vft_feat_packet->n_cams; i++) {
            n_received += vft_feat_packet->num_feats[i];
        }
        if (n_received <= 0) {
            destroy_vft_memory(&vft_feat_packet, &vft_feats);
            return;
        }
    }
    #endif
    else if (c[ch].type == TYPE_RAW) {
        if (en_debug) {
            printf("read raw packet with number of bytes: %d\n", bytes);
        }
        n_received = 1;
    } else {
        fprintf(stderr, "ERROR in simple callback, unknown type\n");
    }

    // if we are printing every packet, loop through each
    for (i = 0; i < n_received; i++) {
        // make sure we don't need to quit
        if (!main_running) return;

        // skip samples if necessary
        if (c[ch].n_to_skip > 0) {
            if (c[ch].n_skipped >= c[ch].n_to_skip) {
                c[ch].n_skipped = 0;
            } else {
                c[ch].n_skipped++;
                if (en_debug) {
                    printf(
                        "skipping sample ch: %d n_skipped: %d, n_to_skip: %d",
                        ch, c[ch].n_skipped, c[ch].n_to_skip);
                }
                continue;
            }
        }

        // print everything in one go.
        int ret = 0;
        if (c[ch].type == TYPE_IMU) {
            ret = fprintf(c[ch].csv_fd,
                          "%d,%ld,%d,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.2f\n",
                          c[ch].n_samples,
                          imu_array[i].timestamp_ns,
                          imu_batch_idx,
                          (double)imu_array[i].accl_ms2[0],
                          (double)imu_array[i].accl_ms2[1],
                          (double)imu_array[i].accl_ms2[2],
                          (double)imu_array[i].gyro_rad[0],
                          (double)imu_array[i].gyro_rad[1],
                          (double)imu_array[i].gyro_rad[2],
                          (double)imu_array[i].temp_c);
        } else if (c[ch].type == TYPE_VIO) {
            vio_data_t d = vio_array[i];
            float roll, pitch, yaw, roll_cam, pitch_cam, yaw_cam;
            _rotation_to_tait_bryan(d.R_imu_to_vio, &roll, &pitch, &yaw);
            _rotation_to_tait_bryan_xyz_intrinsic(d.R_cam_to_imu, &roll_cam,
                                                  &pitch_cam, &yaw_cam);
            fprintf(c[ch].csv_fd, "%d,%ld,", c[ch].n_samples,
                    vio_array[i].timestamp_ns);
            fprintf(c[ch].csv_fd, "%0.4f,%0.4f,%0.4f,",
                    (double)d.T_imu_wrt_vio[0], (double)d.T_imu_wrt_vio[1],
                    (double)d.T_imu_wrt_vio[2]);
            fprintf(c[ch].csv_fd, "%0.4f,%0.4f,%0.4f,", (double)roll,
                    (double)pitch, (double)yaw);
            fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,",
                    (double)d.vel_imu_wrt_vio[0], (double)d.vel_imu_wrt_vio[1],
                    (double)d.vel_imu_wrt_vio[2]);
            fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,",
                    (double)d.imu_angular_vel[0], (double)d.imu_angular_vel[1],
                    (double)d.imu_angular_vel[2]);
            fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,",
                    (double)d.gravity_vector[0], (double)d.gravity_vector[1],
                    (double)d.gravity_vector[2]);
            fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,",
                    (double)d.T_cam_wrt_imu[0], (double)d.T_cam_wrt_imu[1],
                    (double)d.T_cam_wrt_imu[2]);
            fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,", (double)roll_cam,
                    (double)pitch_cam, (double)yaw_cam);
            ret = fprintf(c[ch].csv_fd, "%d,%d,%d,%d\n", d.n_feature_points,
                          d.quality, d.state, d.error_code);
        } else if (c[ch].type == TYPE_OV_EVAL) {
            vio_data_t d = vio_array[i];
            float tb[3] = {0};
            double q[4] = {0};
            _rotation_to_tait_bryan(d.R_imu_to_vio, &tb[0], &tb[1], &tb[2]);
            // **note** this quat has imaginary part first, need that last tho
            rc_quaternion_from_tb_array(tb, q);
            // time, x, y, z, qx, qy, qz, qw
            ret = fprintf(c[ch].csv_fd,
                          "%ld,%0.4f,%0.4f,%0.4f,%0.4f,%0.4f,%0.4f,%0.4f\n",
                          vio_array[i].timestamp_ns, (double)d.T_imu_wrt_vio[0],
                          (double)d.T_imu_wrt_vio[1],
                          (double)d.T_imu_wrt_vio[2], q[1], q[2], q[3], q[0]);
        } else if (c[ch].type == TYPE_POSE_6DOF) {
            pose_vel_6dof_t p = pose_6dof_array[i];
            _write_int(ch, c[ch].n_samples);
            _write_int64(ch, p.timestamp_ns);
            _write_float_3(ch, p.T_child_wrt_parent);
            _write_R_as_tb(ch, p.R_child_to_parent);
            _write_float_3(ch, p.v_child_wrt_parent);
            _write_float_3(ch, p.w_child_wrt_child);
            ret = fprintf(c[ch].csv_fd, "\n");
        } else if (c[ch].type == TYPE_TOF) {
            // first write the index and timestamp to csv
            ret = fprintf(c[ch].csv_fd, "%d,%ld\n", c[ch].n_samples,
                          tof_array[i].timestamp_ns);

            // then write the raw data from the pipe to the raw file
            int n_written =
                fwrite(&tof_array[i], sizeof(tof_data_t), 1, c[ch].raw_fd);
            if (n_written != 1) ret = -1;
        } else if (c[ch].type == TYPE_TOF2) {
            // first write the index and timestamp to csv
            ret = fprintf(c[ch].csv_fd, "%d,%ld\n", c[ch].n_samples,
                          tof2_array[i].timestamp_ns);

            // then write the raw data from the pipe to the raw file
            int n_written =
                fwrite(&tof2_array[i], sizeof(tof2_data_t), 1, c[ch].raw_fd);
            if (n_written != 1) ret = -1;
        } else if (c[ch].type == TYPE_MAV) {
            // first write the index and timestamp to csv
            ret = fprintf(c[ch].csv_fd, "%d,%ld\n", c[ch].n_samples,
                          _time_monotonic_ns());

            // then write the raw data from the pipe to the raw file
            int n_written = fwrite(&mavlink_array[i], sizeof(mavlink_message_t),
                                   1, c[ch].raw_fd);
            if (n_written != 1) ret = -1;
        } else if (c[ch].type == TYPE_QVIO) {
            ext_vio_data_t* d = &ext_vio_array[i];

            _write_int(ch, d->v.quality);
            _write_int64(ch, d->v.timestamp_ns);
            _write_int(ch, d->last_cam_frame_id);
            _write_int64(ch, d->last_cam_timestamp_ns);
            _write_float_3(ch, d->v.T_imu_wrt_vio);
            _write_R_as_tb(ch, d->v.R_imu_to_vio);
            _write_cov_6x6_tri(ch, d->v.pose_covariance);

            _write_float(ch, d->imu_cam_time_shift_s);
            _write_float_3(ch, d->v.vel_imu_wrt_vio);
            _write_cov_3x3_tri(ch, d->v.velocity_covariance);

            _write_float_3(ch, d->v.imu_angular_vel);
            _write_float_3(ch, d->v.gravity_vector);
            _write_cov_3x3(ch, d->gravity_covariance);

            _write_float_3(ch, d->gyro_bias);
            _write_float_3(ch, d->accl_bias);

            _write_float_3(ch, d->v.T_cam_wrt_imu);
            _write_R_as_tb_xyz_intrinsic(ch, d->v.R_cam_to_imu);

            _write_int(ch, d->v.error_code);
            _write_int(ch, d->v.n_feature_points);
            _write_int(ch, d->v.state);
            _write_int(ch, d->n_total_features);

            // group the feature quality, depth, and location at the beginning
            // to make reading the log in a spreadsheet easier
            for (j = 0; j < VIO_MAX_REPORTED_FEATURES; j++) {
                _write_int(ch, d->features[j].point_quality);
            }
            for (j = 0; j < VIO_MAX_REPORTED_FEATURES; j++) {
                _write_float(ch, d->features[j].depth);
            }
            for (j = 0; j < VIO_MAX_REPORTED_FEATURES; j++) {
                _write_float(ch, d->features[j].depth_error_stddev);
            }
            for (j = 0; j < VIO_MAX_REPORTED_FEATURES; j++) {
                _write_float(ch, d->features[j].pix_loc[0]);
                _write_float(ch, d->features[j].pix_loc[1]);
            }

            // dump the rest of the data at the end
            for (j = 0; j < VIO_MAX_REPORTED_FEATURES; j++) {
                _write_int(ch, d->features[j].id);
                _write_float_3(ch, d->features[j].tsf);
                _write_cov_3x3(ch, d->features[j].p_tsf);
            }

            ret = fprintf(c[ch].csv_fd, "\n");
        }  // end of qvio write
        #ifndef BUILD_APQ8096
        else if (c[ch].type == TYPE_VFT_FP) {
            ret = fprintf(c[ch].csv_fd,
                          "%ld,%d,%d,%d,%ld,%0.2f,%0.2f,%0.2f,%0.2f,%d,%d,%d,",
                          vft_feat_packet->timestamp_ns[0],
                          vft_feat_packet->reset,
                          vft_feat_packet->frame_ids[0],
                          vft_feats[i].cam_id,
                          vft_feats[i].id,
                          vft_feats[i].x,
                          vft_feats[i].y,
                          vft_feats[i].x_prev,
                          vft_feats[i].y_prev,
                          vft_feats[i].age,
                          vft_feats[i].score,
                          vft_feats[i].pyr_lvl_mask);
            for(size_t j = 0; j < 32; j++) {
                ret = fprintf(c[ch].csv_fd, "%02X", vft_feats[i].descriptor[j]);
            }
            ret = fprintf(c[ch].csv_fd, "\n");
        }
        #endif
        else if (c[ch].type == TYPE_RAW) {
            // first write the index and number of bytes to csv
            ret = fprintf(c[ch].csv_fd, "%d,%d\n", c[ch].n_samples, bytes);

            // then write the raw data from the pipe to the raw file
            int n_written = fwrite(data, bytes, 1, c[ch].raw_fd);

            if (n_written != 1) ret = -1;
        }

        else {
            fprintf(stderr, "ERROR in simple callback, unknown type\n");
        }

        // check for write errors
        if (ret < 0) {
            perror("ERROR writing to disk");
            c[ch].running = 0;
        }

        // increment counter and check if we are finished
        c[ch].n_samples++;
        if (c[ch].sample_limit > 0 && c[ch].n_samples >= c[ch].sample_limit) {
            c[ch].running = 0;
            printf("channel %d reached sample limit, stopping itself\n", ch);
            return;
        }
    }

    #ifndef BUILD_APQ8096
    // if the channel logs vft features, then the memory needs to freed
    if (c[ch].type == TYPE_VFT_FP) {
        destroy_vft_memory(&vft_feat_packet, &vft_feats);
    }
    #endif

    // sleep a bit so we don't thrash the disk. The pipe will handle buffering
    // the data while we sleep.

    // this sleep was commented out so that imu batches could be given an id so
    // that replaying the imu data would be more accuate
    // usleep(100000);

    return;
}

#define OUTPUT_PNG 0
#define OUTPUT_JPG 1

static void _construct_cam_path(int ch, int i, char* path, int output_format) {
    if (output_format == OUTPUT_JPG) {
        sprintf(path, "%s%s%05d.jpg", log_dir, c[ch].pipe_path, i);
    } else if (output_format == OUTPUT_PNG) {
        sprintf(path, "%s%s%05d.png", log_dir, c[ch].pipe_path, i);
    } else {
        fprintf(stderr, "ERROR in %s, unknown output_format\n", __FUNCTION__);
    }
    return;
}

static void _construct_stereo_path(int ch, int i, char* path_l, char* path_r,
                                   int output_format) {
    if (output_format == OUTPUT_JPG) {
        sprintf(path_l, "%s%s%05dl.jpg", log_dir, c[ch].pipe_path, i);
        sprintf(path_r, "%s%s%05dr.jpg", log_dir, c[ch].pipe_path, i);
    } else if (output_format == OUTPUT_PNG) {
        sprintf(path_l, "%s%s%05dl.png", log_dir, c[ch].pipe_path, i);
        sprintf(path_r, "%s%s%05dr.png", log_dir, c[ch].pipe_path, i);
    } else {
        fprintf(stderr, "ERROR in %s, unknown output_format\n", __FUNCTION__);
    }
    return;
}

#define FMT_INVALID 0
#define FMT_YUV 2
#define FMT_NV 3

static int tj_fmt_from_mpa(camera_image_metadata_t meta, int* outfmt) {
    switch (meta.format) {
        case IMAGE_FORMAT_YUV420:
            *outfmt = TJSAMP_420;
            return FMT_YUV;

        case IMAGE_FORMAT_YUV422:
            *outfmt = TJSAMP_422;
            return FMT_YUV;

        case IMAGE_FORMAT_NV21:
        case IMAGE_FORMAT_NV12:
            *outfmt = TJSAMP_420;
            return FMT_NV;

        case IMAGE_FORMAT_STEREO_NV12:
        case IMAGE_FORMAT_STEREO_NV21:
            *outfmt = TJSAMP_420;
            return FMT_NV;

        default:
            return FMT_INVALID;
    }
}

// Converts a nv12 image to YUV420
static void _cvt_nv_yuv(unsigned char* frame, int width, int height,
                        int isNV12) {
    int y_size = width * height;

    unsigned char* uv_tmp = (unsigned char*)malloc(y_size / 2);
    memcpy(uv_tmp, frame + y_size, (y_size / 2));

    unsigned char* u = frame + y_size;
    unsigned char* v = frame + (y_size * 5 / 4);

    for (int i = 0; i < y_size / 4; i++) {
        if (isNV12) {  // nv12
            u[i] = uv_tmp[(i * 2)];
            v[i] = uv_tmp[(i * 2) + 1];
        } else {  // nv21
            u[i] = uv_tmp[(i * 2) + 1];
            v[i] = uv_tmp[(i * 2)];
        }
    }
    free(uv_tmp);
}

static void _float32_to_uint8(char* frame, int w, int h) {
    // basically doing the same thing as a call to cv normalize (i think)
    // iterate through the pointer, keeping track of global min/max
    // then just normalize to 0-1, and scale by 255
    float max = 0;
    float min = std::numeric_limits<float>::max();

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            if (frame[(x * w) + y] > max) max = frame[(x * w) + y];
            if (frame[(x * w) + y] < min) min = frame[(x * w) + y];
        }
    }

    float scale_factor = max / min * 255;

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            frame[(x * w) + y] = frame[(x * w) + y] * scale_factor;
        }
    }
}

// returns:
// 0 on success
// -1 on turbojpeg compress error
// -2 on file open error
// -3 on file write error
static int turbojpeg_save_image(camera_image_metadata_t meta, char* frame,
                                char image_path[512]) {
    if (frame == nullptr) return -1;

    int ret = 0;
    /* BEGIN JPEG COMPRESS */
    tjhandle tjInstance = NULL;
    unsigned long jpegSize = 0;
    unsigned char* jpegBuf = NULL; /* Dynamically allocate the JPEG buffer */
    int outSubsamp;
    int outQual = 100;
    int flags = 0;
    // speed up flags
    flags |= TJFLAG_FASTUPSAMPLE;
    flags |= TJFLAG_FASTDCT;

    int width = meta.width;
    int height = meta.height;

    switch (tj_fmt_from_mpa(meta, &outSubsamp)) {
        case FMT_NV:

            _cvt_nv_yuv((unsigned char*)frame, width, height,
                        meta.format == IMAGE_FORMAT_NV12);
            // intentionally fall through this case!

        case FMT_YUV:

            if ((tjInstance = tjInitCompress()) == NULL)
                THROW_TJ("initializing compressor");

            if (tjCompressFromYUV(tjInstance, (uint8_t*)frame, width, 1, height,
                                  outSubsamp, &jpegBuf, &jpegSize, outQual,
                                  flags) < 0)
                THROW_TJ("compressing image");

            tjDestroy(tjInstance);
            tjInstance = NULL;
            /* END JPEG COMPRESS */
            break;

        case FMT_INVALID:
        default:
            ret = -1;
            break;
    }

    if (jpegBuf == NULL) ret = -1;

    // dont even try to save the image if we had issues
    if (ret == 0) {
        FILE* file = fopen(image_path, "wb");
        if (!file) {
            fprintf(stderr, "Failed to open jpeg file %s.\n", image_path);
            ret = -2;
        }
        unsigned int n_written = fwrite(jpegBuf, 1, jpegSize, file);
        if (n_written != jpegSize) ret = -3;
        fclose(file);
    }

    tjFree(jpegBuf);
    return ret;
}

unsigned int crc_table[256];

static void make_crc_table() {
    unsigned int c;
    for (int n = 0; n < 256; n++) {
        c = (unsigned int)n;
        for (int k = 0; k < 8; k++) {
            if (c & 1)
                c = 0xedb88320L ^ (c >> 1);
            else
                c = c >> 1;
        }
        crc_table[n] = c;
    }
}

static unsigned int calculate_crc32(unsigned int crc, const unsigned char *buf, size_t len) {
    crc = crc ^ 0xffffffffL;
    while (len--) {
        crc = crc_table[(crc ^ (*buf++)) & 0xff] ^ (crc >> 8);
    }
    return crc ^ 0xffffffffL;
}

// Adler-32 calculation function
static unsigned long adler32(unsigned long adler, const unsigned char *buf, size_t len) {
    unsigned long s1 = adler & 0xffff;
    unsigned long s2 = (adler >> 16) & 0xffff;
    for (size_t n = 0; n < len; n++) {
        s1 = (s1 + buf[n]) % 65521;
        s2 = (s2 + s1) % 65521;
    }
    return (s2 << 16) + s1;
}

static int libpng_save_image(char* frame, int format, int w, int h,
                             char image_path[512]) {
    if (frame == nullptr) return -1;

    int64_t cmp_time = _time_monotonic_ns();

    FILE *fp = fopen(image_path, "wb");
    if (!fp) {
        fprintf(stderr, "Error: Cannot open file %s for writing.\n", image_path);
        return -1;
    }

    // Initialize write structure
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        fprintf(stderr, "Error: png_create_write_struct failed.\n");
        fclose(fp);
        return -1;
    }

    // Initialize info structure
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        fprintf(stderr, "Error: png_create_info_struct failed.\n");
        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        fclose(fp);
        return -1;
    }

    // Set error handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error: during png creation.\n");
        png_destroy_write_struct(&png_ptr, &info_ptr);
        fclose(fp);
        return -1;
    }

    png_init_io(png_ptr, fp);

    // Set image attributes
    int color_type = PNG_COLOR_TYPE_GRAY;
    int bit_depth = 8;
    int bytes_per_pixel = 1;

    if (format == PNG_FORMAT_GRAY) {
        color_type = PNG_COLOR_TYPE_GRAY;
        bit_depth = 8;
        bytes_per_pixel = 1;
    } else if (format == PNG_FORMAT_LINEAR_Y) {
        color_type = PNG_COLOR_TYPE_GRAY;
        bit_depth = 16;
        bytes_per_pixel = 2;
    } else if (format == PNG_FORMAT_RGB) {
        color_type = PNG_COLOR_TYPE_RGB;
        bit_depth = 8;
        bytes_per_pixel = 3;
    } else {
        fprintf(stderr, "Error: Unsupported format: %d\n", format);
        png_destroy_write_struct(&png_ptr, &info_ptr);
        fclose(fp);
        return -1;
    }

    png_set_IHDR(
        png_ptr,
        info_ptr,
        w,
        h,
        bit_depth,
        color_type,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_BASE,
        PNG_FILTER_TYPE_BASE
    );

    // Set compression level to zero (no compression)
    png_set_compression_level(png_ptr, 0);

    // Write header (must be done before writing the image data)
    png_write_info(png_ptr, info_ptr);

    // Allocate memory for row pointers
    png_bytep *row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * h);
    if (!row_pointers) {
        fprintf(stderr, "Error: Failed to allocate memory for row pointers.\n");
        png_destroy_write_struct(&png_ptr, &info_ptr);
        fclose(fp);
        return -1;
    }

    if (format == PNG_FORMAT_LINEAR_Y) {
        // Convert rows to big-endian if necessary
        for (int y = 0; y < h; y++) {
            uint16_t* row = (uint16_t*)(frame + y * w * 2);
            for (int x = 0; x < w; x++) {
                row[x] = htons(row[x]); // Convert to big-endian
            }
            row_pointers[y] = (png_bytep)row;
        }
    } else {
        // Set the individual row pointers to point to the correct offsets of frame
        for (int y = 0; y < h; y++) {
            row_pointers[y] = (png_bytep)(frame + y * w * bytes_per_pixel);
        }
    }

    // Write image data
    png_write_image(png_ptr, row_pointers);

    // End write
    png_write_end(png_ptr, NULL);

    // Clean up
    free(row_pointers);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);

    return 0;
}

static int prev_frame_id[PIPE_CLIENT_MAX_CHANNELS];

// camera frame callback registered to voxl-camera-server
static void _cam_cb(__attribute__((unused)) int ch,
                    camera_image_metadata_t meta, char* frame,
                    __attribute__((unused)) void* context) {
    // if we've been told to stop running, just return
    if (c[ch].running == 0) return;

    if (only_record_on_arm && !is_armed)
        return;

    // TODO how backed up do we want to allow the pipe to get
    if(pipe_client_bytes_in_pipe(ch) > pipe_client_get_pipe_size(ch) / 2){
		printf("flushing backed up pipe ch %d\n", ch);
		pipe_client_flush(ch);

        c[ch].dropped_data++;
        
		return;
	}

    // if file hasn't been updated yet, open it!
    if (c[ch].csv_fd == 0) {
        if (_start_csv(ch)) {
            return;
        }
    }

    // skip samples if necessary
    if (c[ch].n_to_skip > 0) {
        if (c[ch].n_skipped >= c[ch].n_to_skip) {
            c[ch].n_skipped = 0;
        } else {
            c[ch].n_skipped++;
            if (en_debug) {
                printf(
                    "skipping camera frame ch: %d n_skipped: %d, n_to_skip: %d",
                    ch, c[ch].n_skipped, c[ch].n_to_skip);
            }
            return;
        }
    }

    int w = meta.width;
    int h = meta.height;
    char img_path1[512];
    char img_path2[512];

    if (prev_frame_id[ch] > 0 && meta.frame_id - 1 != prev_frame_id[ch]) {
        c[ch].dropped_data++;
    } 
    prev_frame_id[ch] = meta.frame_id;

    // make opencv Mat images and write to disk
    if (meta.format == IMAGE_FORMAT_RAW8) {
        _construct_cam_path(ch, c[ch].n_samples, img_path1, OUTPUT_PNG);

        if (libpng_save_image(frame, PNG_FORMAT_GRAY, w, h, img_path1) < 0) {
            fprintf(stderr, "ERROR: failed to save png at %s\n", img_path1);
            return;
        }
    } else if (meta.format == IMAGE_FORMAT_STEREO_RAW8) {
        _construct_stereo_path(ch, c[ch].n_samples, img_path1, img_path2,
                               OUTPUT_PNG);

        if (libpng_save_image(frame, PNG_FORMAT_GRAY, w, h, img_path1) < 0) {
            fprintf(stderr, "ERROR: failed to save png at %s\n", img_path1);
            return;
        }
        if (libpng_save_image(frame + (w * h), PNG_FORMAT_GRAY, w, h,
                              img_path2) < 0) {
            fprintf(stderr, "ERROR: failed to save png at %s\n", img_path2);
            return;
        }
    } else if (meta.format == IMAGE_FORMAT_RAW16) {
        _construct_cam_path(ch, c[ch].n_samples, img_path1, OUTPUT_PNG);

        if (libpng_save_image(frame, PNG_FORMAT_LINEAR_Y, w, h, img_path1) <
            0) {
            fprintf(stderr, "ERROR: failed to save png at %s\n", img_path1);
            return;
        }
    } else if (meta.format == IMAGE_FORMAT_FLOAT32) {
        // who knows what the range of numbers will be for floating point inputs
        // so scale it with minmax method to 0-255 for normal 8-bit output
        // TODO how to do this such that it can be replayed?
        _float32_to_uint8(frame, w, h);
        // dont worry about replaying the data
        _construct_cam_path(ch, c[ch].n_samples, img_path1, OUTPUT_PNG);
        if (libpng_save_image(frame, PNG_FORMAT_GRAY, w, h, img_path1) < 0) {
            fprintf(stderr, "ERROR: failed to save png at %s\n", img_path1);
            return;
        }
    } else if (meta.format == IMAGE_FORMAT_NV12 ||
               meta.format == IMAGE_FORMAT_NV21) {
        _construct_cam_path(ch, c[ch].n_samples, img_path1, OUTPUT_JPG);
        if (turbojpeg_save_image(meta, frame, img_path1) < 0) {
            fprintf(stderr, "failed to save jpeg at %s.\n", img_path1);
            return;
        }

    } else if (meta.format == IMAGE_FORMAT_STEREO_NV12 ||
               meta.format == IMAGE_FORMAT_STEREO_NV21) {
        _construct_stereo_path(ch, c[ch].n_samples, img_path1, img_path2,
                               OUTPUT_JPG);
        if (turbojpeg_save_image(meta, frame, img_path1) < 0) {
            fprintf(stderr, "failed to save jpeg at %s.\n", img_path1);
            return;
        }
        if (turbojpeg_save_image(meta, frame + (meta.size_bytes / 2),
                                 img_path2) < 0) {
            fprintf(stderr, "failed to save jpeg at %s.\n", img_path2);
            return;
        }
    } else if (meta.format == IMAGE_FORMAT_RGB) {
        _construct_cam_path(ch, c[ch].n_samples, img_path1, OUTPUT_PNG);

        if (libpng_save_image(frame, PNG_FORMAT_RGB, w, h, img_path1) < 0) {
            fprintf(stderr, "ERROR: failed to save png at %s\n", img_path1);
            return;
        }
    }

    else {
        fprintf(stderr,
                "ERROR only support RAW8, RAW16, NV12/21, RGB, and FLOAT32 "
                "images right now\n");
        fprintf(stderr, "got %s instead\n",
                pipe_image_format_to_string(meta.format));
        return;
    }

    // print metadata to csv
    // NOTE all fields after 'w' were added later, replay needs to be updated to
    // read both
    int ret =
        fprintf(c[ch].csv_fd, "%d,%ld,%d,%d,%d,%d,%d,%d,%d\n", c[ch].n_samples,
                meta.timestamp_ns, meta.gain, meta.exposure_ns, meta.format, h,
                w, meta.frame_id, meta.reserved);

    // check for write errors
    if (ret < 0) {
        perror("ERROR writing to disk");
        c[ch].running = 0;
    }

    // increment counter and check if we are finished
    c[ch].n_samples++;
    if (c[ch].sample_limit > 0 && c[ch].n_samples >= c[ch].sample_limit) {
        c[ch].running = 0;
        printf("channel %d reached sample limit, stopping itself\n", ch);
        return;
    }

    return;
}

static void _mavlink_onboard_helper_cb(__attribute__((unused)) int ch,
        char *data, int bytes, __attribute__((unused)) void *context)
{
    static int arm_debounce = 0;

    if (main_running == 0)
    {
        return;
    }

    mavlink_message_t *mav_msg = (mavlink_message_t*) data;
    if (mav_msg->msgid == MAVLINK_MSG_ID_HEARTBEAT)
    {
        // get ARMED state
        bool t_arm = (mavlink_msg_heartbeat_get_system_status(mav_msg) == MAV_STATE_ACTIVE);

        if (t_arm != is_armed)
        {
            arm_debounce++;
        if (arm_debounce > 4 || t_arm)   // bias to arming, debounce while armed
                is_armed = t_arm;
        }
        else
        {
            arm_debounce = 0;
        }

        if (!is_armed && is_operating)
        {
            printf("Disarmed and done!\n");
            main_running = 0;
            return;
        }
    }

        // TBD if logging based on throttle up
    if (mav_msg->msgid == MAVLINK_MSG_ID_VFR_HUD)
    {
        if (mavlink_msg_vfr_hud_get_throttle(mav_msg) > 0 && is_armed && !is_operating)
        {
            is_operating = true;
        }
    }
}


// servers may not necessarily be up and running when the logger starts, so keep
// trying untill they are.
// TODO remove this in favor of AUTO_RECONNECT mode
static void* _pipe_opener(void* arg) {
    int ret;
    int ch = (long)arg;

    // keep trying to open the requested pipe as long as main is still running
    // and the helper thread hasn't closed itself
    while (main_running && c[ch].running) {
        if (c[ch].type == TYPE_IMU) {
            pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
            // attempt to connect to the server expecting it might fail
            ret = pipe_client_open(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
                                   EN_PIPE_CLIENT_SIMPLE_HELPER,
                                   IMU_RECOMMENDED_READ_BUF_SIZE);
            if (ret == 0) {
                // great! we connected to server
                printf("connected to ch%2d imu server: %s\n", ch,
                       c[ch].pipe_path);
                return NULL;
            }
            if (en_debug)
                printf("failed to connect to server: %s trying again\n",
                       c[ch].pipe_path);
        }

        else if (c[ch].type == TYPE_CAM) {
            // attempt to connect to the server expecting it might fail
            ret = pipe_client_open(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
                                   EN_PIPE_CLIENT_CAMERA_HELPER, 0);
            if (ret == 0) {
                // great! we connected to server
                printf("connected to ch%2d cam server: %s\n", ch,
                       c[ch].pipe_path);
                pipe_client_set_camera_helper_cb(ch, _cam_cb, NULL);
                return NULL;
            }
            if (en_debug)
                printf("failed to connect to server: %s trying again\n",
                       c[ch].pipe_path);
        }

        else if (c[ch].type == TYPE_POSE_6DOF) {
            // attempt to connect to the server expecting it might fail
            ret = pipe_client_open(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
                                   EN_PIPE_CLIENT_SIMPLE_HELPER,
                                   POSE_6DOF_RECOMMENDED_READ_BUF_SIZE);
            if (ret == 0) {
                // great! we connected to server
                printf("connected to ch%2d pose6DOF server: %s\n", ch,
                       c[ch].pipe_path);
                pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
                return NULL;
            }
            if (en_debug)
                printf("failed to connect to server: %s trying again\n",
                       c[ch].pipe_path);
        }

        else if (c[ch].type == TYPE_VIO || c[ch].type == TYPE_OV_EVAL) {
            // attempt to connect to the server expecting it might fail
            ret = pipe_client_open(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
                                   EN_PIPE_CLIENT_SIMPLE_HELPER,
                                   VIO_RECOMMENDED_READ_BUF_SIZE);
            if (ret == 0) {
                // great! we connected to server
                printf("connected to ch%2d vio server: %s\n", ch,
                       c[ch].pipe_path);
                pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
                return NULL;
            }
            if (en_debug)
                printf("failed to connect to server: %s trying again\n",
                       c[ch].pipe_path);
        }

        else if (c[ch].type == TYPE_TOF || c[ch].type == TYPE_TOF2) {
            // attempt to connect to the server expecting it might fail
            ret = pipe_client_open(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
                                   EN_PIPE_CLIENT_SIMPLE_HELPER,
                                   TOF2_RECOMMENDED_READ_BUF_SIZE);
            if (ret == 0) {
                // great! we connected to server
                printf("connected to ch%2d PMD TOF: %s\n", ch, c[ch].pipe_path);
                pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
                return NULL;
            }
            if (en_debug)
                printf("failed to connect to server: %s trying again\n",
                       c[ch].pipe_path);
        }

        else if (c[ch].type == TYPE_MAV) {
            // attempt to connect to the server expecting it might fail
            ret = pipe_client_open(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
                                   EN_PIPE_CLIENT_SIMPLE_HELPER,
                                   MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
            if (ret == 0) {
                // great! we connected to server
                printf("connected to ch%2d mavlink: %s\n", ch, c[ch].pipe_path);
                pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
                return NULL;
            }
            if (en_debug)
                printf("failed to connect to server: %s trying again\n",
                       c[ch].pipe_path);
        }

        else if (c[ch].type == TYPE_QVIO) {
            // attempt to connect to the server expecting it might fail
            ret = pipe_client_open(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
                                   EN_PIPE_CLIENT_SIMPLE_HELPER,
                                   EXT_VIO_RECOMMENDED_READ_BUF_SIZE);
            if (ret == 0) {
                // great! we connected to server
                printf("connected to ch%2d qvio server: %s\n", ch,
                       c[ch].pipe_path);
                pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
                return NULL;
            }
            if (en_debug)
                printf("failed to connect to server: %s trying again\n",
                       c[ch].pipe_path);
        }

        #ifndef BUILD_APQ8096
        else if (c[ch].type == TYPE_VFT_FP) {
            // attempt to connect to the server expecting it might fail
            // 64K is the Linux Kernel default pipe size.
            ret = pipe_client_open(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
                                   EN_PIPE_CLIENT_SIMPLE_HELPER,
                                   sizeof(vft_feature_packet));
            if (ret == 0) {
                // great! we connected to server
                printf("connected to ch%2d vft_fp server: %s\n", ch,
                       c[ch].pipe_path);
                pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
                return NULL;
            }
            if (en_debug)
                printf("failed to connect to server: %s trying again\n",
                       c[ch].pipe_path);
        }
        #endif

        else if (c[ch].type == TYPE_RAW) {
            // attempt to connect to the server expecting it might fail
            // 64K is the Linux Kernel default pipe size.
            ret = pipe_client_open(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
                                   EN_PIPE_CLIENT_SIMPLE_HELPER, (64 * 1024));
            if (ret == 0) {
                // great! we connected to server
                printf("connected to ch%2d raw server: %s\n", ch,
                       c[ch].pipe_path);
                pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
                return NULL;
            }
            if (en_debug)
                printf("failed to connect to server: %s trying again\n",
                       c[ch].pipe_path);
        }

        else {
            fprintf(stderr, "ERROR in _pipe_opener unsupported type\n");
            return NULL;
        }
        usleep(100000);
    }

    return NULL;
}

static void _update_json(int update_time) {
    if (en_debug) printf("updating json file\n");

    if (update_time) {
        // update running duration
        int64_t duration_ns = _time_monotonic_ns() - start_time;
        cJSON_SetNumberValue(duration_json, (double)duration_ns / 1000000000.0);
    }

    int i;
    for (i = 0; i < n_ch; i++) {
        cJSON* array_item = cJSON_GetArrayItem(channel_array, i);
        if (array_item == NULL) {
            fprintf(stderr, "ERROR, can't find channel array item in json\n");
        }
        cJSON* sample_obj = cJSON_GetObjectItem(array_item, "n_samples");
        if (sample_obj == NULL) {
            fprintf(stderr, "ERROR, can't find n_samples item in json\n");
        }
        cJSON_SetIntValue(sample_obj, c[i].n_samples);

        // TOF can change type from TOF->TOF2 so want it to be updated
        cJSON* type_obj = cJSON_GetObjectItem(array_item, "type");
        if (type_obj == NULL) {
            fprintf(stderr, "ERROR, can't find type_string item in json\n");
        }
        cJSON_SetIntValue(type_obj, c[i].type);
        cJSON* type_str_obj = cJSON_GetObjectItem(array_item, "type_string");
        if (array_item == NULL) {
            fprintf(stderr, "ERROR, can't find type_string item in json\n");
        }
        cJSON_SetValuestring(type_str_obj, _type_to_string(c[i].type));
    }
    // finish writing out final totals for samples
    if (en_debug) {
        printf("writing info.json to disk\n");
    }
    json_write_to_file(info_path, parent);
    return;
}

// pthread that stops the program after time
static void* _stop_timer(__attribute__((unused)) void* context) {
    printf("starting %0.1f second timer\n", (double)timeout_s);
    usleep(timeout_s * 1000000);
    printf("stopping automatically after requested timeout\n");
    main_running = 0;
    return NULL;
}

int main(int argc, char* argv[]) {
    int i;

    // start by parsing arguments
    if (_parse_opts(argc, argv)) return -1;

    // start signal handler so we can exit cleanly
    if (enable_signal_handler() == -1) {
        fprintf(stderr, "ERROR: failed to start signal handler\n");
        return -1;
    }

    // start by making sure the base directory exists
    if (en_debug) printf("making sure base directory exists:%s\n", base_dir);
    _mkdir(base_dir);

    // search for existing log folders to determine the next number in the
    // series
    for (i = 0; i <= 10000; i++) {
        // make the next log directory in the sequence
        sprintf(log_dir, "%slog%04d/", base_dir, i);
        if (en_debug) printf("check if previous log dir exists: %s\n", log_dir);

        // use basic mkdir to try making this directory
        int ret = mkdir(log_dir, S_IRWXU);
        if (ret == 0) {
            if (en_debug) printf("successfully made new dir: %s\n", log_dir);
            break;
        }
        if (errno != EEXIST) {
            perror("error searching for next log directory in sequence");
            return -1;
        }
        // dir exists, move onto the next one
    }

    // make a json info file to describe whats contained in this log
    if (en_debug) printf("starting json object\n");
    parent = cJSON_CreateObject();
    if (parent == NULL) return -1;
    // start by recording that we are on log format version 1. In the future
    // if we need to change the format we can use this to differentiate
    cJSON_AddNumberToObject(parent, "log_format_version", 1);
    // put user-specified note and the total number of channels at the top
    cJSON_AddStringToObject(parent, "note", note);
    cJSON_AddNumberToObject(parent, "n_channels", n_ch);
    start_time = _time_monotonic_ns();
    cJSON_AddNumberToObject(parent, "start_time_monotonic_ns", start_time);

    char datestring[128];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(datestring, "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900,
            tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    cJSON_AddStringToObject(parent, "start_time_date", datestring);
    duration_json = cJSON_AddNumberToObject(parent, "duration_s", 0.0);

    // create an array of objects describing each channel
    channel_array = cJSON_CreateArray();
    cJSON_AddItemToObject(parent, "channels", channel_array);
    for (i = 0; i < n_ch; i++) {
        if (en_debug) printf("adding ch %d to json\n", i);
        cJSON* new_channel = cJSON_CreateObject();
        cJSON_AddNumberToObject(new_channel, "channel", i);
        cJSON_AddNumberToObject(new_channel, "n_samples",
                                -1);  // -1 indicates unknown
        cJSON_AddNumberToObject(new_channel, "n_to_skip", c[i].n_to_skip);
        if (c[i].type == TYPE_CAM) {
            cJSON_AddBoolToObject(new_channel, "dropped_data", c[i].dropped_data > 0);
        }
        cJSON_AddNumberToObject(new_channel, "type", c[i].type);
        cJSON_AddStringToObject(new_channel, "type_string",
                                _type_to_string(c[i].type));
        cJSON_AddStringToObject(new_channel, "pipe_path", c[i].pipe_path);
        // put the final object into the array
        cJSON_AddItemToArray(channel_array, new_channel);
    }

    // write out to disk with starting values for now. The parent cJSON object
    // will be updated as we go and the final numbers will be re-written to disk
    sprintf(info_path, "%sinfo.json", log_dir);
    if (en_debug) printf("writing json to file: %s\n", info_path);
    if (json_write_to_file(info_path, parent)) return -1;

    // copy over config and calibration files
    char cmd[512];
    sprintf(cmd, "%sdata/modalai/", log_dir);
    _mkdir(cmd);
    sprintf(cmd, "cp -R /data/modalai/*.yml %sdata/modalai/ 2>/dev/null", log_dir);
    system(cmd);
    sprintf(cmd, "%setc/modalai/", log_dir);
    _mkdir(cmd);
    sprintf(cmd, "cp -R /etc/modalai/*.conf %setc/modalai/", log_dir);
    system(cmd);

    // set main running flag to 1 so the threads about to be created don't just
    // exit right away
    main_running = 1;

    if (only_record_on_arm)
    {
        // check for arming
        pipe_client_set_simple_helper_cb(MAV_STATUS_CH,
            _mavlink_onboard_helper_cb, NULL);
        if (pipe_client_open(MAV_STATUS_CH, "mavlink_onboard", "mavlink_onboard_helper", CLIENT_FLAG_EN_SIMPLE_HELPER,
            2048) != 0)
        {
        fprintf(stderr, "failed to open MAVLINK\n");
        return(-1);
        }
    }





    // start threads to open all the pipes
    for (i = 0; i < n_ch; i++) {
        c[i].running = 1;
        if (en_debug) printf("starting pipe opener thread for channel %d\n", i);
        pthread_create(&c[i].thread_id, NULL, _pipe_opener, (void*)(long)i);
    }

    // main updates the json file and decides when to close up
    // the running flag will only go from 1 to 0 if the channel thread closes
    // by itself due to error or reaching the sample limit, so we can check the
    // running flag to see if all of the channels closed themselves.

    if (en_debug) printf("entering main loop\n");

    if (timeout_s > 0) {
        pthread_create(&timeout_thread, NULL, _stop_timer, NULL);
    }
    while (main_running) {
        // check for the condition where all the threads stopped themselves
        int have_all_stopped = 1;
        for (i = 0; i < n_ch; i++) {
            if (c[i].running != 0) {
                have_all_stopped = 0;
                break;
            }
        }
        if (have_all_stopped) {
            if (en_debug) printf("all helpers stopped themselves\n");
            break;
        }

        // wait a bit
        usleep(100000);

        // update the json file with running tally of samples
        _update_json(1);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // close everything
    ////////////////////////////////////////////////////////////////////////////////

    // wait a bit for opener threads to close and things to write to disk
    usleep(500000);

    // close all the pipes
    if (en_debug) printf("closing pipes\n");
    pipe_client_close_all();

    // flush and close all csv files
    if (en_debug) printf("flushing and closing csv files\n");
    for (i = 0; i < n_ch; i++) {
        if (c[i].csv_fd != 0) {
            fflush(c[i].csv_fd);
            fclose(c[i].csv_fd);
        }
        if (c[i].raw_fd != 0) {
            fflush(c[i].raw_fd);
            fclose(c[i].raw_fd);
        }
    }

    // final update of running sample totals to the json file
    _update_json(0);

    // print some helpful info to the screen
    printf("summary of data logged:\n");
    for (i = 0; i < n_ch; i++) {
        printf("ch%2d, type: %s, samples: %7d, path: %s\n", i,
               _type_to_string(c[i].type), c[i].n_samples, c[i].pipe_path);
    }

    printf("created new log here:\n%s\n", log_dir);
    return 0;
}
